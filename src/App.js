import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <br></br>
      <br></br>
      <br></br>
      <div class="row">
        <div class="col-sm-6">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Nuevo proyecto</h5>
              <p class="card-text">Crea un nuevo proyecto para calcular la duración aproximada y costo aproximado de un proyecto de sotfware ágil</p>
              <a href="#" class="btn btn-primary">Crear nuevo proyecto</a>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Lista de proyectos</h5>
              <p class="card-text">Ver la lista de proyectos creados y estimados</p>
              <a href="#" class="btn btn-primary">Ver proyectos</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
