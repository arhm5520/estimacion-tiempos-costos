# EstimacionTiemposCostos

## Herramienta de apoyo para la estimación continua de tiempos y costos en el desarrollo de software ágil

## Objetivo principal
Proponer una nueva herramienta de apoyo para calcular la duración y el costo aproximado de un proyecto de software ágil a partir de la estimación continua de esfuerzo del producto backlog o puntos del proyecto (PY), la velocidad en puntos del equipo por mes o puntos por mes (PM) y el costo del equipo de trabajo (C1). Esto con el fin de reducir los riesgos asociados a la estimación de tiempos y costos tanto en la negociación de un proyecto de software como en todo su ciclo de vida del desarrollo de software destinado a garantizar la actualización de las estimaciones a partir de las modificaciones en el producto backlog y costos del equipo, siendo así una herramienta de apoyo para actualizar las condiciones dadas con el cliente. 

